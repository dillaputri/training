package com.example.demo.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.repository.PositionRepo;

@RestController
public class MainController {

	@Autowired
	private EmployeesRepo er;
	
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		return employees;
	}
	
	@GetMapping("/getempall")
		public List<Employees> getEmpAll(){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}
	
	@GetMapping("/getempposid")
	public List<Employees> getEmPosId(@RequestParam Integer posID){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posID);
		return empls;
	}
      
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
	try {
		e = er.save(e);
		respMap.put("empID", e.getEmpID().toString());
		respMap.put("code",HttpStatus.OK+"");
		respMap.put("msg", "success"); 
	} catch (Exception e2) {
		//TODO: handle exception
		respMap.put("empID", "");
		respMap.put("code",HttpStatus.CONFLICT+"");
		respMap.put("msg", "failed");
		
	}
	return respMap;

	
	}
	
	@PostMapping("/delete")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
	try {
		er.delete(e);
		respMap.put("empID", e.getEmpID().toString());
		respMap.put("code",HttpStatus.OK+"");
		respMap.put("msg", "success"); 
	} catch (Exception e2) {
		//TODO: handle exception
		respMap.put("empID", "");
		respMap.put("code",HttpStatus.CONFLICT+"");
		respMap.put("msg", "failed");
		
	}
	return respMap;

	
	}
	
	@GetMapping("/getage")
	public List<Employees> getEmpAge(@RequestParam Integer age){
		List<Employees> empage = new ArrayList<Employees>();
		empage = (List<Employees>) er.findByAge(age);
		return empage;
	}
	
	@GetMapping("/getgen")
	public List<Employees> getEmpGen(@RequestParam String gender){
		List<Employees> empgen = new ArrayList<Employees>();
		empgen = (List<Employees>) er.findByGender(gender);
		return empgen;
	}
	
	@Autowired
	private PositionRepo ep;
	
	@GetMapping("/getposid")
	public Position getByPosId(@RequestParam Integer posID) {
		Position position = new Position();
		position = ep.findByPosID(posID);
		return position;
	}
	
	@GetMapping("/getposall")
	public List<Position> getPosAll(){
	List<Position> posts = new ArrayList<Position>();
	posts = (List<Position>) ep.findAll();
	return posts;
}
	 @GetMapping("/getposname")
	 public Position getByPosName(@RequestParam String posName) {
	  Position position = new Position();
	  position = ep.findByPosName(posName);
	  return position;
	 }
	 @GetMapping("/getallowance")
	 public List<Position> getByAllowance(@RequestParam Integer allowance){
	  List<Position> empls = new ArrayList<Position>();
	  empls = (List<Position>) ep.findByAllowance(allowance);
	  return empls; 
	 }
	 @PostMapping("setpos")
	 public Map<String, String> setPos(@RequestBody Position p)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	   p = ep.save(p);
	   
	   respMap.put("empID", p.getPosID(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }
	 @PostMapping("delpos")
	 public Map<String, String> delPos(@RequestBody Position p)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	    ep.delete(p);
	   
	   respMap.put("empID", p.getPosID(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }
	 
	 @Autowired
		private AttendanceRepo ea;
	

	 
	 @GetMapping("/getattid")
	 public Attendance getByAttenId(@RequestParam Integer attenId) {
	  Attendance attendance = new Attendance();
	  attendance = ea.findByAttenId(attenId);
	  return attendance;
	 }
	 @GetMapping("/getattempid")
	 public List<Attendance> getAttEmpId(@RequestParam Integer empId){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ea.findByEmpID(empId);
	  return empls; 
	 }
	 @GetMapping("/getattdate")
	 public List<Attendance> getAttDate(@RequestParam Date attDate){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ea.findByAttDate(attDate);
	  return empls; 
	 }
	 @GetMapping("/getattstatus")
	 public List<Attendance> getAttStatus(@RequestParam String attStatus){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ea.findByAttStatus(attStatus);
	  return empls; 
	 }
	 @GetMapping("/getattall")
	 public List<Attendance> getAttAll(){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ea.findAll();
	  return empls;
	 }
	 @PostMapping("setatt")
	 public Map<String, String> setAtt(@RequestBody Attendance a)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	   a = ea.save(a);
	   
	   respMap.put("empID", a.getAttenId(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }
	 
	 @PostMapping("delatt")
	 public Map<String, String> delAtt(@RequestBody Attendance a)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	   ea.delete(a);
	   
	   respMap.put("empID", a.getAttenId(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }
	
	
	
	
	
	
}
	
	
